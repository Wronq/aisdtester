﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AiSDTester.Startup))]
namespace AiSDTester
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
